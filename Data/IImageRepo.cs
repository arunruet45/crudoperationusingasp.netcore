using System.Collections.Generic;
using CrudOperation.Models;

namespace CrudOperation.Data
{
    public interface IImageRepo {

        bool SaveChanges();
        IList<Image> getFIlteredImage(Image imageFilterProperties);
    }
}