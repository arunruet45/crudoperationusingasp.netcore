using System.Collections.Generic;
using System.Linq;
using CrudOperation.Models;
using CurdOperation.Data;
using Microsoft.EntityFrameworkCore;

namespace CrudOperation.Data 
{
    public class SqlImageRepo : IImageRepo
    {   
        private CrudOperationContext _context;
        public SqlImageRepo(CrudOperationContext context) {
            _context = context;
        }

        public IList<Image> getFIlteredImage(Image imageFilterProperties)
        {
            var images = _context.Images.FromSqlRaw("Select * from dbo.Images").ToList();
            return images ;
        }

        // public IList<Image> getFIlteredImage(Image imageFilterProperties)
        // {
        //     IList<Image> images = _context.Images.
        //     return images; 
        // }

        public bool SaveChanges()
        {
            return(_context.SaveChanges() >= 0); 
        }
    }
}