using System.Collections.Generic;
using CrudOperation.Models;

namespace CrudOperation.Data {
    
    public interface IPersonRepo {

        bool SaveChanges();
        Person GetPersonByid(int id);
        IEnumerable<Person> GetAllPersons();
        void AddPerson(Person person);
        void UpdatePerson(int id, Person person);
        void PartialUpdatePerson(int id, Person person);
        void DeletePerson(Person person);
    }
}