using CrudOperation.Models;
using Microsoft.EntityFrameworkCore;

namespace CurdOperation.Data
{
    public class CrudOperationContext : DbContext
    {
        public CrudOperationContext(DbContextOptions<CrudOperationContext> opt) : base(opt) {

        }

        public DbSet<Person> Persons { get; set; }
        public DbSet<Image> Images { get; set; }
    }
}