using System.Collections.Generic;
using CrudOperation.Models;
using CrudOperation.Data;
using System;
using CurdOperation.Data;
using System.Linq;

namespace crudOperation.Data {
    public class SqlPersonRepo : IPersonRepo {
        private CrudOperationContext _context;
        public SqlPersonRepo(CrudOperationContext context) {
            _context = context;
        }

        public bool SaveChanges() {
            return (_context.SaveChanges() >= 0);
        }
        
        public IEnumerable<Person> GetAllPersons() {
            return _context.Persons.ToList();
        }

        public Person GetPersonByid(int id) {
            return _context.Persons.FirstOrDefault(person => person.id == id);
        }

        public void AddPerson(Person person) {
            if (person == null) {
                throw new ArgumentNullException(nameof(person));
            }
            _context.Persons.Add(person);
        }

        public void UpdatePerson(int id, Person person) {
            Person personToUpdate  = _context.Persons.FirstOrDefault(person => person.id == id);
            if (personToUpdate == null) {
                throw new ArgumentNullException(nameof(personToUpdate));
            }
            personToUpdate.name = person.name;
            personToUpdate.roll = person.roll;
            personToUpdate.address = person.address;
            _context.Persons.Update(personToUpdate);
        }

        public void PartialUpdatePerson(int id, Person person) {
            //
        }
          
        public void DeletePerson(Person person) {
            if (person == null) {
                throw new ArgumentNullException(nameof(person));
            }
            _context.Persons.Remove(person);
        }
    }
}