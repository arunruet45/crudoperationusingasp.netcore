using System.ComponentModel.DataAnnotations;

namespace CrudOperation.Models
{
    public class Image {
        
        [Key]
        public int id { get; set; }
        public string image { get; set; }
        public string condition { get; set; }
        public string exerciseDifficulty { get; set; }
        public string equipmentAvailable { get; set; }
        public string exerciseType { get; set; }
        public string bodyPart { get; set; }
        public string ageCategory { get; set; }
        public string imageOrientation { get; set; }
        public string selectTextToDisplayWithExerciseImage { get; set; }
    }
}