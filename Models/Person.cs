using System.ComponentModel.DataAnnotations;

namespace CrudOperation.Models {
    public class Person {

        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public int roll { get; set; }
        public string address { get; set; }
    }
}