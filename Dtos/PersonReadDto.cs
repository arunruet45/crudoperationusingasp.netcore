namespace CrudOperation.Dto {
    public class PersonReadDto {

        public int id { get; set; }
        public string name { get; set; }
        public int roll { get; set; }
    }
}