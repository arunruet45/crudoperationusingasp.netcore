using System.Collections.Generic;
using CrudOperation.Data;
using CrudOperation.Models;
using Microsoft.AspNetCore.Mvc;

namespace CrudOperation.Controller
{
    [Route("api/v1/images")]
    [ApiController]
    public class ImageController : ControllerBase {

        private IImageRepo _repository;
        public ImageController(IImageRepo repository) {
            _repository = repository;
        }   

        [HttpGet]
        public ActionResult<IList<Image>> getFilteredImage(Image imageFilterProperties) {
            IList<Image> images = _repository.getFIlteredImage(imageFilterProperties);
            if (images == null) {
                return NoContent();
            }
            return Ok(images);
        }
    }
}