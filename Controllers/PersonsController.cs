using System.Collections.Generic;
using AutoMapper;
using CrudOperation.Data;
using CrudOperation.Dto;
using CrudOperation.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace CrudOperation.Controller
{   
    [Route("api/v1/persons")]
    [ApiController]
    public class PersonsController : ControllerBase
    {
        private IPersonRepo _repository;
        private IMapper _mapper;
        public PersonsController(IPersonRepo repository, IMapper mapper) {
            _repository = repository;
            _mapper = mapper;
        }
        
        [HttpGet]
        public ActionResult<IEnumerable<Person>> FindAll() {
            var persons = _repository.GetAllPersons();
            if (persons == null) {
                return NoContent();
            }
            return Ok(_mapper.Map<IEnumerable<PersonReadDto>>(persons));
        }

        [HttpGet("{id}")]
        public ActionResult<Person> GetPersonByid(int id) {
            Person person = _repository.GetPersonByid(id);
            if(person == null) {
                return NoContent();
            }
            return person;
        }

        [HttpPost]
        public ActionResult<Person> CreatePerson(Person person) {
            _repository.AddPerson(person);
            _repository.SaveChanges();
            return Ok();
        }

        [HttpPut("{id}")]
        public ActionResult<Person> UpdatePerson(int id, Person person) {
            Person personToUpdate = _repository.GetPersonByid(id);
            if(personToUpdate == null) {
                return NoContent();
            }
            _repository.UpdatePerson(id, person);
            _repository.SaveChanges();
            return Ok();
        }

        [HttpPatch("{id}")]
        public ActionResult<Person> PartialUpdatePerson(int id, JsonPatchDocument<Person> patchDoc) {
            Person personToPartialUpdate = _repository.GetPersonByid(id);
            if (personToPartialUpdate == null) {
                return NoContent();
            }
            patchDoc.ApplyTo(personToPartialUpdate, ModelState);
            if (!TryValidateModel(personToPartialUpdate)) {
                return ValidationProblem(ModelState);
            }
            _repository.SaveChanges();
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult DeletePerson(int id) {
            Person personToDelete = _repository.GetPersonByid(id);
            if (personToDelete == null) {
                return NoContent();
            }
            _repository.DeletePerson(personToDelete);
            _repository.SaveChanges();
            return Ok();
        }
    }
}