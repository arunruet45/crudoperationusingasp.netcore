using AutoMapper;
using CrudOperation.Dto;
using CrudOperation.Models;

namespace CrudOperation.Profiles
{ 
    public class PersonProfile : Profile {
        public PersonProfile() {
            CreateMap<Person, PersonReadDto>();
        }
    }
}