﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace crudOperation.Migrations
{
    public partial class SecondMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Images",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    image = table.Column<string>(nullable: true),
                    condition = table.Column<string>(nullable: true),
                    exerciseDifficulty = table.Column<string>(nullable: true),
                    equipmentAvailable = table.Column<string>(nullable: true),
                    exerciseType = table.Column<string>(nullable: true),
                    bodyPart = table.Column<string>(nullable: true),
                    ageCategory = table.Column<string>(nullable: true),
                    imageOrientation = table.Column<string>(nullable: true),
                    selectTextToDisplayWithExerciseImage = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Images", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Images");
        }
    }
}
